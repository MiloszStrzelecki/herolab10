#include <iostream>
#include <fstream>
#include <string>
#pragma once
class Monster;

class Hero{
public:
    std::string name;
    int strength;
    int dexterity;
    int endurance;
    int intelligence;
    int charisma;
    
    Hero(std::string new_name, int new_strength, int new_dexterity, int new_endurance, int new_intelligence, int new_charisma);
    
    std::string view();
    
    int save();
    int load();
    
    friend class Type;
    friend class Monster;
    
};
