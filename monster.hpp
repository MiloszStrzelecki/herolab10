#include "hero.hpp"

class Monster{
    std::string name;
    int strength;
    int dexterity;
    int endurance;
    int intelligence;
    int charisma;
public:
    
    std::string m_name(std::string new_name);
    int m_strength(int mon_strength);
    int m_dexterity(int mon_dexterity);
    int m_endurance(int mon_endurance);
    int m_intelligence(int mon_intelligence);
    int m_charisma(int mon_charisma);
    
    int save();
    std::string view();
};
