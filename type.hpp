#include "hero.hpp"

class Type{
    std::string type;
public:
    void set_type(std::string new_type);
    void increase(Hero& c);
};
