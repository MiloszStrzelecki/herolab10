#include "type.hpp"

void Type::set_type(std::string new_type){
    type = new_type;
}
void Type::increase(Hero& c){
    if(type == "Mage"){
        c.intelligence+=10;
    }else if (type == "Warrior"){
        c.endurance += 10;
    }else if (type == "Berserker"){
        c.strength +=10;
    }else if (type == "Thief"){
        c.dexterity +=10;
    }
}
